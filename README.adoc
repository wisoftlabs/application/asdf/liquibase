:author: wisoftlab
:email: <contact@wisoft.io>
:revision: 2021.27.0
:icons: font
:main-title: ASDF Plugin
:sub-title: Liquibase
:description: Liquibase plugin for asdf version manager
:git_service: https://git.wisoft.io/wisoft-dev/asdf/
:project_name: liquibase
:project_license: Copyright
:experimental:
:hardbreaks:


= {main-title}: {sub-title}

image:https://img.shields.io/badge/version-{revision}-blue.svg[link="./CHANGELOG",title="version"]  image:https://img.shields.io/badge/license-{project_license}-lightgrey.svg[link="./LICENSE",title="license"]


== What is the Project

{description}


== Status

Version {revision}

* link:./CHANGELOG[History]
* link:{git_service}{project_name}/boards[Issues Board] / link:{git_service}{project_name}/issues[Issues List]


== Building

. Clone a copy of the repository:
+
[subs="attributes"]
----
$ git clone {git_service}{project_name}.git
----
+

. Change to the project directory:
+
[subs="attributes"]
----
$ cd {project_name}
----
+


== Usage

=== Install

Liquibase requires a Java runtime to be installed.

[subs="attributes"]
----
$ asdf plugin-add liquibase {git_service}{project_name}.git
----

=== Use

Check [asdf](https://github.com/asdf-vm/asdf) readme for instructions on how to install & manage versions of Liquibase.


== License

* LICENSE: link:./LICENSE[{project_license}]


== Contributors

* {author} {email}
